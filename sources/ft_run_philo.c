/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_run_philo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 13:11:42 by akazian           #+#    #+#             */
/*   Updated: 2014/05/10 18:42:51 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include <libft.h>
#include <philo.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void	*run_life(void *arg)
{
	t_philo	*philo;

	philo = (t_philo *)arg;
	state_of_philo(philo);
	philo->state = DIE;
	return (0);
}

int		free_list(t_philo *list[NB_PHILO], pthread_mutex_t s[][NB_PHILO])
{
	int	i;

	i = 0;
	while (i < NB_PHILO)
	{
		pthread_mutex_destroy(&s[0][i]);
		free(list[i]);
		i++;
	}
	return (1);
}
