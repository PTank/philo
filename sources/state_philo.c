/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   state_philo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 12:14:42 by akazian           #+#    #+#             */
/*   Updated: 2014/05/09 15:00:38 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <philo.h>
#include <unistd.h>
#include <time.h>

int	sleep_ready(t_philo *philo, int stick1, int stick2)
{
	int		test;

	test = 0;
	if (!pthread_mutex_trylock(&philo->stick[0][stick1]))
		test++;
	if (!pthread_mutex_trylock(&philo->stick[0][stick2]))
		test++;
	if (test == 2)
	{
		philo->state = EAT;
		philo->eat_tm = (int)time(NULL);
	}
	else if (test)
	{
		philo->state = THINKS;
		philo->think_tm = (int)time(NULL);
		pthread_mutex_unlock(&philo->stick[0][stick1]);
		pthread_mutex_unlock(&philo->stick[0][stick2]);
	}
	return (0);
}

int	eat_ready(t_philo *philo, int stick1, int stick2)
{
	if ((int)time(NULL) - philo->eat_tm > EAT_T)
	{
		philo->state = SLEEP;
		philo->sleep_tm = (int)time(NULL);
		philo->life = MAX_LIFE;
		pthread_mutex_unlock(&philo->stick[0][stick1]);
		pthread_mutex_unlock(&philo->stick[0][stick2]);
	}
	return (0);
}

int	think_ready(t_philo *philo)
{
	if ((int)time(NULL) - philo->think_tm > THINK_T && (philo->state = SLEEP))
		philo->sleep_tm = 0;
	return (0);
}

int	state_of_philo(t_philo *philo)
{
	int		tmp_tm;
	int		stick1;
	int		stick2;

	stick1 = (philo->name == NB_PHILO) ? 0 : philo->name;
	stick2 = philo->name - 1;
	philo->tm = (int)time(NULL);
	while (philo->exit && philo->life > 0 && !(tmp_tm = 0))
	{
		tmp_tm = (int)time(NULL) - philo->tm;
		if (philo->state == SLEEP && philo->sleep_tm == 0)
			sleep_ready(philo, stick1, stick2);
		else if (philo->state == EAT)
			eat_ready(philo, stick1, stick2);
		else if (philo->state == THINKS)
			think_ready(philo);
		else if (philo->state == SLEEP)
		{
			if (philo->sleep_tm && (int)time(NULL) - philo->sleep_tm > REST_T)
				philo->sleep_tm = 0;
		}
		if (philo->state != EAT && tmp_tm && (philo->life -= tmp_tm))
			philo->tm = (int)time(NULL);
	}
	return (0);
}
