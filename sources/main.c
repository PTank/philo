/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/06 11:43:29 by akazian           #+#    #+#             */
/*   Updated: 2014/05/10 18:38:31 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <philo.h>
#include <unistd.h>
#include <time.h>

t_philo	*init_philosophe(int id, pthread_mutex_t chopstick[][NB_PHILO])
{
	t_philo	*new;

	if (!(new = ft_memalloc(sizeof(t_philo))))
		return (NULL);
	new->name = id;
	new->life = MAX_LIFE;
	new->state = SLEEP;
	new->sleep_tm = 0;
	new->stick = chopstick;
	new->exit = 1;
	return (new);
}

void	*run_timeout(void *arg)
{
	t_env	*e;
	int		i;
	int		x;

	e = (t_env *)arg;
	x = 1;
	while ((i = -1) && x && time(NULL) - e->tm < TIMEOUT + 1 && ft_putline())
	{
		while (++i < NB_PHILO)
		{
			if (e->list[i]->state == EAT)
				ft_putid(PHILO, i + 1, " \033[1;35meat\033[1;30m	");
			else if (e->list[i]->state == SLEEP)
				ft_putid(PHILO, i + 1, " \033[1;34msleep\033[1;30m");
			else if (e->list[i]->state == THINKS)
				ft_putid(PHILO, i + 1, " \033[1;33mthink\033[1;30m");
			else if (e->list[i]->state == DIE && !(x = 0))
				ft_putid(PHILO, i + 1, " \033[1;31mdie\033[1;30m	");
			ft_putid("	pv: \033[32m", e->list[i]->life, "\033[1;30m	|\n");
		}
		ft_putendline();
		usleep(40000);
	}
	exit_thread(e, x);
	return (0);
}

void	ft_timeout(t_philo *list[NB_PHILO + 1])
{
	t_env	*e;

	e = ft_memalloc(sizeof(t_env));
	list[NB_PHILO] = init_philosophe(NB_PHILO, NULL);
	e->list = list;
	e->tm = (int)time(NULL);
	if (pthread_create(&list[NB_PHILO]->id, NULL, run_timeout, e))
		ft_putendl_fd("Can't create thread", 2);
}

int		init_stick(pthread_mutex_t chopstick[][NB_PHILO])
{
	int	i;

	i = 0;
	while (i < NB_PHILO)
	{
		pthread_mutex_init(&chopstick[0][i], NULL);
		i++;
	}
	return (0);
}

int		main(void)
{
	int				name;
	t_philo			*list[NB_PHILO + 1];
	pthread_mutex_t	chopstick[NB_PHILO];

	init_stick(&chopstick);
	name = 0;
	while (name < NB_PHILO)
	{
		name++;
		list[name - 1] = init_philosophe(name, &chopstick);
		if (pthread_create(&list[name - 1]->id, NULL, run_life, list[name - 1]))
			ft_putendl_fd("Can't create thread", 2);
	}
	name = 0;
	ft_timeout(list);
	while (name < NB_PHILO)
	{
		pthread_join(list[name]->id, NULL);
		usleep(30);
		name++;
	}
	if (list[0]->end)
		return (ft_putret(DANCE, &chopstick, list));
	return (ft_putret("\033[31mTHEY DIE\033[0m", &chopstick, list));
}
