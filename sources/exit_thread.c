/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_thread.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 14:18:46 by akazian           #+#    #+#             */
/*   Updated: 2014/05/10 16:12:23 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>
#include <stdlib.h>

int	exit_thread(t_env *e, int ret)
{
	int	i;

	i = 0;
	while (i < NB_PHILO && !(e->list[i]->exit = 0))
		i++;
	e->list[0]->end = ret;
	free(e);
	return (1);
}
