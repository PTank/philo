/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tool.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 13:11:26 by akazian           #+#    #+#             */
/*   Updated: 2014/05/10 18:49:37 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <unistd.h>
#include <philo.h>

int	ft_putid(char *str1, int id, char *str2)
{
	ft_putstr(str1);
	ft_putnbr(id);
	ft_putstr(str2);
	return (1);
}

int	ft_putline(void)
{
	ft_putstr("\033[2J");
	ft_putendl("------ \033[31mINTERFACE GRAPHIQUE\033[1;30m ------");
	ft_putendl("|				|");
	return (1);
}

int	ft_putret(char *str, pthread_mutex_t s[][NB_PHILO], t_philo *list[NB_PHILO])
{
	char	buff[2048];

	ft_putendl(str);
	ft_putstr("Press enter to exit ");
	ft_bzero(buff, 2048);
	while (read(0, &buff, 2048))
	{
		if (*(unsigned int *)buff == 10)
			break ;
		ft_bzero(buff, 2048);
		ft_putstr("Press enter to exit ");
	}
	free_list(list, s);
	return (0);
}

int	ft_putendline(void)
{
	ft_putendl("|				|");
	ft_putendl("---------------------------------");
	return (1);
}
