/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/07 15:31:25 by akazian           #+#    #+#             */
/*   Updated: 2014/05/10 18:42:22 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _PHILO_H
# define _PHILO_H

# include <pthread.h>
# define NB_PHILO 7

# define MAX_LIFE 15
# define EAT_T 2
# define TIMEOUT 30
# define THINK_T 1
# define REST_T 4

# define PHILO "|   \033[1;30mphilo "
# define DANCE "\033[32mNow, it is time... To DAAAAAAAANCE ! ! !\033[0m"

# include <struct_philo.h>

void	*run_life(void *arg);
int		ft_putid(char *str1, int id, char *str2);
int		state_of_philo(t_philo *philo);
int		ft_putline(void);
int		exit_thread(t_env *e, int ret);
int		ft_putret(char *r, pthread_mutex_t s[][NB_PHILO], t_philo *l[NB_PHILO]);
int		free_list(t_philo *list[NB_PHILO], pthread_mutex_t s[][NB_PHILO]);
int		ft_putendline(void);
#endif
