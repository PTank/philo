/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_philo.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akazian <akazian@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/09 15:05:53 by akazian           #+#    #+#             */
/*   Updated: 2014/05/09 15:06:42 by akazian          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _STRUCT_PHILO_H
# define _STRUCT_PHILO_H

typedef enum		e_state
{
	THINKS,
	SLEEP,
	EAT,
	DIE
}					t_state;

typedef struct		s_philo
{
	pthread_t		id;
	pthread_mutex_t	(*stick)[NB_PHILO];
	int				name;
	int				life;
	int				tm;
	int				think_tm;
	int				sleep_tm;
	int				eat_tm;
	int				state;
	int				exit;
	int				end;
}					t_philo;

typedef struct		s_env
{
	t_philo			**list;
	int				tm;
}					t_env;

#endif
