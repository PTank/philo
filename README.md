#*Historique*:
[wikipedia](http://fr.wikipedia.org/wiki/D%C3%AEner_des_philosophes)

##*Cour:*
* [pcouvee.free](http://pcouvee.free.fr/ch7_6_philosophes_02v1.pdf)
* [msdn.microsoft.com](http://msdn.microsoft.com/fr-fr/magazine/dd882512.aspx)(explication avec du C++)
* [tonguim.free.fr/](http://tonguim.free.fr/cours/systdexp/semaphoresParadigmes.pdf)
* [achrafothman.net/](http://www.achrafothman.net/docs/sys.expl.II.td.3.pdf)
* [clerentin.iut-amiens.fr/](http://clerentin.iut-amiens.fr/LP_RGI/TD6_philo.pdf)(woua
le pdf full color)
* [ensiwiki.ensimag.fr](https://ensiwiki.ensimag.fr/images/8/85/Pb_philosophes.pdf)

##*Semaphore:*
* [wikipedia ](http://fr.wikipedia.org/wiki/S%C3%A9maphore_%28informatique%29)
* [jean-luc.massat.perso.luminy.univ-amu.fr/](http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/docs/thread-sem.html)

##*Mutex:*
* [top-news.fr](http://top-news.fr/utilisation-des-mutex-exemple-thread-linux/)
* [thegeekstuff.com](http://www.thegeekstuff.com/2012/05/c-mutex-examples/)
* [openclassrooms.com/](http://fr.openclassrooms.com/informatique/cours/la-programmation-systeme-en-c-sous-unix/qu-est-ce-qu-un-thread)
